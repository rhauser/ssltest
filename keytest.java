
import sun.security.x509.*;

import java.security.cert.*;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.cert.Certificate;
import java.util.Date;
import java.io.IOException;


class KeyTest {

    private static X509Certificate generateCertificate(KeyStore ks, String dn, int days)
            throws GeneralSecurityException, IOException
    {
        String algorithm = "SHA256withECDSA";

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
        keyPairGenerator.initialize(spec);
        KeyPair pair = keyPairGenerator.generateKeyPair();

        PrivateKey privkey = pair.getPrivate();
        X509CertInfo info = new X509CertInfo();
        Date from = new Date();
        Date to = new Date(from.getTime() + days * 86400000l);
        CertificateValidity interval = new CertificateValidity(from, to);
        X500Name owner = new X500Name(dn);

        info.set(X509CertInfo.VALIDITY, interval);
        info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(1));
        info.set(X509CertInfo.SUBJECT, owner);
        info.set(X509CertInfo.ISSUER, owner);
        info.set(X509CertInfo.KEY, new CertificateX509Key(pair.getPublic()));
        info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
        AlgorithmId algo = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);
        info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));

        // Sign the cert to identify the algorithm that's used.
        X509CertImpl cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);

        // Update the algorith, and resign.
        algo = (AlgorithmId) cert.get(X509CertImpl.SIG_ALG);
        info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, algo);
        cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);

        String password = "tdaqpw";
        ks.setKeyEntry("tdaq", privkey, password.toCharArray(), new Certificate[] { cert });

        return cert;
    }

    public static void main (String[] argv) throws Exception {

        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);

        String distinguishedName = "CN=Java, OU=TDAQ, O=ATLAS";
        Certificate certificate = generateCertificate(ks, distinguishedName, 365);
        System.out.println("it worked!");
    }
}
