
import ipc.Partition;
import ipc.servant;
import is.InfoList;

import org.jacorb.orb.ORB;
import org.jacorb.orb.etf.ListenEndpoint;
import org.jacorb.orb.etf.ProtocolAddressBase;

class JTest {

  public static void main(String args[])
  {
      Partition p = new Partition();
      System.out.println("name = " + p.getName());
      System.out.println("is valid = " + p.isValid());
      try {
          ipc.servant s = p.lookup("is/repository", "Test");

          org.jacorb.orb.ORB orb = (org.jacorb.orb.ORB)ipc.Core.getORB();
          System.out.println("ior = " + orb.object_to_string(s));

          InfoList l = new InfoList(p, "Test");
          for(int i = 0; i < l.getSize(); i++) {
              System.out.println("name = " + l.getName(i));
              System.out.println("info = " + l.getInfo(i));
          }

          // org.jacorb.orb.giop.TransportManager mgr = orb.getTransportManager();
          // java.util.ArrayList<ListenEndpoint>	eps =
          //     mgr.getListenEndpoints(ListenEndpoint.Protocol.SSLIOP);
          // for(ListenEndpoint ep : eps ) {
          //     System.out.println("endpoint = " + ep.getAddress());
          //     System.out.println("SSL endpoint = " + ep.getSSLAddress());
          // }
      } catch(java.lang.Exception e) {
          System.out.println("No Test");
      }
  }
}
